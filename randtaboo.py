import numpy as np
import random
import sys
import collections
from itertools import product

size = (400, 400)
tries = size[0]*size[1]/5
shots_per_try = 100

center = (size[0]/2, size[1]/2)
radius = min(*center)

def write_PGM(array, stream):
    stream.write("P5\n%d %d\n255\n" % array.shape)
    stream.write(array.tostring())

def dist2(p1, p2):
    return (p1[0]-p2[0])**2+(p1[1]-p2[1])**2

def rect_rand():
    return (
            random.randint(0, size[0]-1),
            random.randint(0, size[1]-1)
            )
def circle_rand():
    while True:
        r=rect_rand()
        if dist2(center, r)<radius*radius:
            return r

def do_rand_taboo(taboo_size, randfn):
    points=collections.deque([], taboo_size)

    img = np.zeros(size, dtype=np.uint8)

    for i in range(tries):
        pt=None
        if len(points)==0:
            pt=randfn()
        else:
            pts = [randfn() for _ in xrange(shots_per_try)]
            pt = max(pts, key=lambda p: min(dist2(p, p2) for p2 in points))
        points.append(pt)
        img[pt[0],pt[1]]=i*255/tries

    write_PGM(img, open("%s-%03d.pgm" % (randfn.__name__, taboo_size), "w"))

randfns={'c': circle_rand, 'r': rect_rand}
fn=randfns[sys.argv[1][0].lower()]
min_taboo=int(sys.argv[2])
max_taboo=int(sys.argv[3])

for i in range(min_taboo, max_taboo):
    print i
    do_rand_taboo(i, fn)
